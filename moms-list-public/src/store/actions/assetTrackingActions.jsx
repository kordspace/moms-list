import firebase from "config/fbConfig.js"
import axios from "axios";

export const startTracking = (billNum) => {
  return (dispatch) => {
    let acc = {}

    // fetch delivery info from firebase
    const db = firebase.firestore()
    var docRef = db.collection("TESTbills").doc(billNum)

    docRef.get().then((doc) => {
      if (doc.exists) {
        const data = doc.data()
                
        // Format Firebase Delivery Info for mapping    
        acc = {
          destination: {
            lat: data.destination.latitude,
            lng: data.destination.longitude
          },
          origin: {
            lat: data.origin.latitude,
            lng: data.origin.longitude
          },
          truckNum: data.truckNum         
        }        
      } else {
        // doc.data() will be undefined in this case
        dispatch(mapError())
      }
    }).then(() => {
      // request keep trucking for delivery info       
      let url = `https://galvanize-cors.herokuapp.com/https://api.keeptruckin.com/v1/vehicle_locations?vehicle_ids=${acc.truckNum}`
      axios.get(
        url,
        {
          "headers": {
            "x-api-key": "04f166c5-61be-4e9d-bd18-b35452c55b80"
          }
        }
      ).then((response) => {
        // add truck location to accumulator
        const location = response.data.vehicles[0].vehicle.current_location
        
        acc = {
          ...acc,
          truckLocation: {
            lat: location.lat,
            lng: location.lon
          }
        }
        dispatch(tracking(acc))       
      })
        .catch((error) => {
          console.log(error);
        })
    }).catch(function (error) {
      console.log("Error getting document:", error);
    });
  }
}

export const tracking = (deliveryInfo = {}) => ({
  type: "TRACK",
  deliveryInfo
})

export const mapError = () =>({
  type: "MAP_ERROR"
})

export const addMapData = (data = {}) => ({
  type: "ADD_MAP_DATA",
  data
})

// export const addExpense = (expense) => ({
//   type: 'ADD_EXPENSE',
//   expense
// })

// export const startAddExpense = (expenseData = {}) => {
//   return (dispatch, getState) => { // Only works with Thunk
//     const uid = getState().auth.uid
//     const {
//       description = '',
//       note = '',
//       amount = 0,
//       createdAt = 0
//     } = expenseData
//     const expense = { description, note, amount, createdAt }

//     return database.ref(`users/${uid}/expenses`).push(expense).then((ref) => {
//       dispatch(addExpense({
//         id: ref.key,
//         ...expense
//       }))
//     })
//   }
// }