// Info from firestore
// const firestoreData = {
//     traceNum: 69,
//     truckNum: 420, // 6 digit
//     origin: { lat: 39.790486, lng: -104.945051 }, // Where truck last left
//     waypoints: [
//         {
//           location: {lat: 39.407369, lng: -107.225464}, // Destination(s) before Trace Number Destination
//           stopover: true
//         },
//         {
//           location: {lat: 36.102215, lng: -115.199805},
//           stopover: true
//         }
//     ],
//     destination: {lat: 32.746683, lng: -117.150401}, // Destination for Trace Number
//     distance: null
// }

// // Info from keepTruckin:
// const keepTruckinData = {
// truckLocation: {lat: 39.562084, lng: -107.297923}
// }

// const assetTrackingDefaultState = {
//   ...firestoreData,
//   ...keepTruckinData
// }

// For info from keep trucking Input for Google Maps
const trackingLocationDataReducer = (
  state = {},
  action
) => {
  switch (action.type) {
    case 'TRACK':
      console.log("TRACK")
      return { ...action.deliveryInfo }      
    default:
      return state
  }
}

// For info from Google Maps for Info Card
const trackingMapDataReducer = (
  state = {
    truckLocation: {
      lat: 39.790453, 
      lng: -104.944990
    }
  },
  action
) => {
  switch (action.type) {    
    case 'MAP_ERROR':
      console.log('MAP_ERROR')
      return {
        ...state,
        mapError: true
      }
    case 'ADD_MAP_DATA':
      console.log("ADD_MAP_DATA: ", action.data)
      return {
        ...action.data
      }   
    default:
      return state
  }
}

export { trackingLocationDataReducer, trackingMapDataReducer }