import {
    defaultFont,
    primaryColor,
    primaryBoxShadow,
    infoColor,
    infoBoxShadow,
    successColor,
    successBoxShadow,
    warningColor,
    warningBoxShadow,
    dangerColor,
    dangerBoxShadow,
    roseColor,
    roseBoxShadow
  } from "assets/jss/material-kit-pro-react.jsx";
  
  const barStyle = theme => ({
    container: {
      zIndex: '99',
    },
    content: {
      color: "#fff",
      fontSize: "1.5rem",
      fontWeight: "900",
      textAlign: "center",
      lineHeight: "2rem",
      padding: "3rem 15% 3rem 15%",
    }
  });
  
  export default barStyle;
  