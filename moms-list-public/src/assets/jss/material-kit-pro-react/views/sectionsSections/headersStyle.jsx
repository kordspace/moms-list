import headerLinksStyle from "assets/jss/material-kit-pro-react/components/headerLinksStyle.jsx";
import {
  container,
  mrAuto,
  title
} from "assets/jss/material-kit-pro-react.jsx";

const headersSection = theme => ({
  ...headerLinksStyle(theme),
  sectionBlank: {
    height: "70px",
    display: "block"
  },
  sectionSplash: {
    height: "100vh",
    backgroundColor: "white",
    zIndex: "99"
  },
  container: {
    ...container,
    zIndex: "2",
    position: "relative",
    "& h1, & h4, & h6": {
      color: "#FFFFFF"
    }
  },
  conatinerHeader2: {
    ...container,
    zIndex: "2",
    position: "relative",
    "& h1, & h4, & h6": {
      color: "#FFFFFF"
    },
    paddingTop: "25vh"
  },
  title,
  pageHeader: {
    position: "relative",
    maxHeight: "1600px",
    backgroundPosition: "50%",
    backgroundSize: "cover",
    margin: "0",
    padding: "0",
    border: "0",
    display: "flex",
    WebkitBoxAlign: "center",
    MsFlexAlign: "center",
    alignItems: "center",
    "&:before": {
      background: "rgba(0, 0, 0, 0.5)"
    },
    "&:after,&:before": {
      position: "absolute",
      zIndex: "1",
      width: "100%",
      height: "100%",
      display: "block",
      left: "0",
      top: "0",
      content: "''"
    }
  },
  iframeContainer: {
    "& > iframe": {
      width: "100%",
      boxShadow:
        "0 16px 38px -12px rgba(0, 0, 0, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
    }
  },
  mrAuto,
  textCenter: {
    textAlign: "center"
  },
  card: {
    marginTop: "60px"
  },
  formControl: {
    margin: "0",
    padding: "8px 0 0 0"
  },
  textRight: {
    textAlign: "right"
  },
  button: {
    margin: "0 !important"
  },
  carouselHero: {
    background: "#000",
    minHeight: "600px",
    zIndex: "89",
  },
  carouselContent1: {
    backgroundImage: "url('https://firebasestorage.googleapis.com/v0/b/ffpo-216413.appspot.com/o/ffpo1.jpg?alt=media&token=f7a4f1f6-b80c-42e4-87ec-0c7ce3b3aee2')",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "top center",
    backgroundSize: "cover",
    backgroundColor: "rgba(0, 0, 0, 0)",
    height: "100vh",
    minHeight: "37.5rem",
    "& img": {
      display: "block",
      objectFit: "cover",
    },
    "& h1": {
      marginTop: "25vh",
      marginLeft: "10%",
      marginRight: "10%",
      fontWeight: "700",
      color: "white",
      textAlign: "left",
      '@media (max-width: 499px)': {
        marginTop: "25vh",
        fontSize: '2.5rem'
      },
      '@media (min-width: 500px)': {
        fontSize: '3rem'
      },
      '@media (min-width: 1000px)': {
        fontSize: '4rem'
      },
      '@media (min-width: 2000px)': {
        fontSize: '4rem'
      },
    },
    "& h2": {
      color: "white",
      marginLeft: "10%",
      marginRight: "10%",
      textAlign: "left",
      '@media (max-width: 499px)': {
        fontSize: '1.5rem'
      },
      '@media (min-width: 500px)': {
        fontSize: '2em'
      },
      '@media (min-width: 1000px)': {
        fontSize: '2em'
      },
      '@media (min-width: 2000px)': {
        fontSize: '2rem'
      },
    }
  },
  carouselContent2: {
    backgroundImage: "url('https://firebasestorage.googleapis.com/v0/b/ffpo-216413.appspot.com/o/ffpo2.jpg?alt=media&token=0a337905-a71c-4856-aacc-0dac1eee2452')",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "top center",
    backgroundSize: "cover",
    backgroundColor: "rgba(0, 0, 0, 0)",
    height: "100vh",
    minHeight: "37.5rem",
    "& img": {
      display: "block",
      objectFit: "cover",
    },
    "& h1": {
      marginTop: "25vh",
      marginLeft: "10%",
      marginRight: "10%",
      fontWeight: "700",
      color: "white",
      textAlign: "left",
      '@media (max-width: 499px)': {
        fontSize: '2.5rem'
      },
      '@media (min-width: 500px)': {
        fontSize: '3rem'
      },
      '@media (min-width: 1000px)': {
        fontSize: '4rem'
      },
      '@media (min-width: 2000px)': {
        fontSize: '4rem'
      },
    },
    "& h2": {
      color: "white",
      marginLeft: "10%",
      marginRight: "10%",
      textAlign: "left",
      '@media (max-width: 499px)': {
        fontSize: '1.5rem'
      },
      '@media (min-width: 500px)': {
        fontSize: '2em'
      },
      '@media (min-width: 1000px)': {
        fontSize: '2em'
      },
      '@media (min-width: 2000px)': {
        fontSize: '2rem'
      },
    }
  },
  carouselContent3: {
    backgroundImage: "url('https://firebasestorage.googleapis.com/v0/b/ffpo-216413.appspot.com/o/ffpo3.jpg?alt=media&token=afda044d-1b8b-482f-bb1d-fd61fb573f81')",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "top center",
    backgroundSize: "cover",
    backgroundColor: "rgba(0, 0, 0, 0)",
    height: "100vh",
    minHeight: "37.5rem",
    "& img": {
      display: "block",
      objectFit: "cover",
    },
    "& h1": {
      marginTop: "25vh",
      marginLeft: "10%",
      marginRight: "10%",
      fontWeight: "700",
      color: "white",
      textAlign: "left",
      '@media (max-width: 499px)': {
        fontSize: '2.5rem'
      },
      '@media (min-width: 500px)': {
        fontSize: '3rem'
      },
      '@media (min-width: 1000px)': {
        fontSize: '4rem'
      },
      '@media (min-width: 2000px)': {
        fontSize: '4rem'
      },
    },
    "& h2": {
      color: "white",
      marginLeft: "10%",
      marginRight: "10%",
      textAlign: "left",
      '@media (max-width: 499px)': {
        fontSize: '1.5rem'
      },
      '@media (min-width: 500px)': {
        fontSize: '2em'
      },
      '@media (min-width: 1000px)': {
        fontSize: '2em'
      },
      '@media (min-width: 2000px)': {
        fontSize: '2rem'
      },
    }
  },
  carouselNavButton: {
    backgroundColor: "rgba(0, 149, 136, 1)",
    display: "block",
    marginTop: "-1rem",
    marginLeft: "10%",
    marginRight: "10%",
    fontSize: "1rem",
    zIndex: "1",
    '@media (max-width: 499px)': {
    },
    '@media (min-width: 500px)': {
    },
    '@media (min-width: 1000px)': {
    },
    '@media (min-width: 2000px)': {
    },
  },
});

export default headersSection;
