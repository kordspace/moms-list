import { 
  bodyText1, 
  bodyText2,  
  header1,
  freightlinerGrey,
  ltrButton
  } from "assets/jss/ltr-styles.jsx" 


const productStyle = {
  ltrButton: {
    ...ltrButton
  },
  section: {
    padding: "70px 0"
  },    
  title: {
    ...header1,     
    marginBottom: 0,
    minHeight: "32px",
    textDecoration: "none",
    textAlign: "center"
  },
  subtitle: {
    ...bodyText1,
    marginTop: 0,
    marginBottom: "1rem",   
    minHeight: "32px",
    textDecoration: "none",
    textAlign: "right"
  },
  quote: {
    ...bodyText1,
    textAlign: "left"
  },
  cardTitle: {
    ...header1,
    textAlign: "center",
    fontSize: "21px"
  },
  cardStyles: {    
    margin: "5px",
    height: "100%",
    background: freightlinerGrey,
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },  
  description: {
    ...bodyText2,
    textAlign: "justify"    
  },
  gridItem: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  }
};

export default productStyle;
