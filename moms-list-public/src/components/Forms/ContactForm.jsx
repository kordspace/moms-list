import React from "react";
import PropTypes from "prop-types";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";

// @material-ui/icons
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";

import styles from "assets/jss/material-kit-pro-react/components/forms/contactFormStyle.jsx";

class Form extends React.Component {
  state = {
    category: '',
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    message: ''
  }
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden"
    };
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    this.timeOutFunction = setTimeout(
      function () {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }
  componentWillUnmount() {
    clearTimeout(this.timeOutFunction);
    this.timeOutFunction = null;
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state);
  }
  render() {
    const { classes } = this.props;
    return (

      <GridContainer justify="center">
        <GridItem xs={12} sm={11}>
          <form onSubmit={this.handleSubmit}>
            <div className={classes.formContainer}>
              <GridContainer justify="center">
                <GridItem xs={11}>
                  <CustomInput
                    labelText="Category"
                    id="category"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      type: "text",
                      onChange: (event) => this.handleChange(event)
                    }}
                  />
                  <GridContainer justify="space-between">
                    <GridItem xs={12} sm={6}>
                      <CustomInput
                        labelText="First Name"
                        id="firstName"
                        formControlProps={{
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "text",
                          onChange: (event) => this.handleChange(event)
                        }}
                      />
                    </GridItem>
                    <GridItem xs={12} sm={6}>
                      <CustomInput
                        labelText="Last Name"
                        id="lastName"
                        formControlProps={{
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "text",
                          onChange: (event) => this.handleChange(event)
                        }}
                      />
                    </GridItem>
                    <GridItem xs={12} sm={6}>
                      <CustomInput
                        labelText="Phone Number"
                        id="phone"
                        formControlProps={{
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "text",
                          onChange: (event) => this.handleChange(event)
                        }}
                      />
                    </GridItem>
                    <GridItem xs={12} sm={6}>
                      <CustomInput
                        labelText="Email"
                        id="email"
                        formControlProps={{
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "email",
                          onChange: (event) => this.handleChange(event)
                        }}
                      />
                    </GridItem>
                  </GridContainer>
                  <GridItem xs={12}>
                    <CustomInput
                      labelText="Message"
                      id="message"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        type: "text",
                        multiline: true,
                        rows: 5,
                        onChange: (event) => this.handleChange(event)
                      }}
                    />
                  </GridItem>                     
                </GridItem>
              </GridContainer>
            <div className={classes.centerContainer}>
              <Button
                className={classes.ltrButton}
                size="lg"
                type="submit"
                rel="noopener noreferrer"
              >
                SUBMIT
                </Button>
            </div>
            </div>
          </form>
        </GridItem>
      </GridContainer>
    );
  }
}

Form.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Form);
