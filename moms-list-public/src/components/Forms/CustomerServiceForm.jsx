import React from "react";
import PropTypes from "prop-types";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";

// @material-ui/icons
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import styles from "assets/jss/material-kit-pro-react/components/forms/customerServiceFormStyle.jsx";

class Form extends React.Component {
  state = {
    proBillNum: '',
    message: ''
  }
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden"
    };
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    this.timeOutFunction = setTimeout(
      function () {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }
  componentWillUnmount() {
    clearTimeout(this.timeOutFunction);
    this.timeOutFunction = null;
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state);
  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.formWrapper}>
        <h2 className={classes.header1}>HAVE SOMETHING TO SAY ABOUT YOUR SHIPMENT ORDER?</h2>
        <h5 className={classes.bodyText1}>Let a customer service representative know by using the chat form below.</h5>
        <GridContainer justify="center">
          <GridItem xs={10} s={8} md={6}>
            <form
              style={{
                textAlign: "center"
              }}
              onSubmit={this.handleSubmit}
            >
              <CustomInput
                labelText="Pro Bill Number"
                id="proBillNum"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  type: "text",
                  onChange: (event) => this.handleChange(event)
                }}
              />
              <CustomInput
                labelText="Message"
                id="message"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  type: "text",
                  multiline: true,
                  rows: 5,
                  onChange: (event) => this.handleChange(event)
                }}
              />
              <Button
                className={classes.ltrButton}
                size="lg"
                type="submit"               
                rel="noopener noreferrer"
              >
                SUBMIT
              </Button>
            </form>
          </GridItem>
        </GridContainer>


      </div>

    );
  }
}

Form.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Form);
