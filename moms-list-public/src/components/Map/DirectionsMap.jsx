/* global google */

import React from 'react';
import { connect } from 'react-redux';
import { compose, withProps, lifecycle } from 'recompose';
import { addMapData } from '../../store/actions/assetTrackingActions'
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer,
  Marker
} from 'react-google-maps';
import {
  darkMapStyles as mapStyles
} from "./MapsStyles";

class WaypointsMap extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    // Determine if using east or west truck icon.
    let truckIcon     
    this.props.trackingLocationData.truckLocation.lng - this.props.trackingLocationData.destination.lng > 0 ?
      truckIcon = "/truck-icon-west.png"
      :
      truckIcon = "/truck-icon-east.png"

    // Add truck location as first waypoint
    const waypoints = [
      {
        location: this.props.trackingLocationData.truckLocation,
        stopover: true
      }
    ]

    const mapDispatchToProps = (dispatch) => ({
      addMapData: (data) => dispatch(addMapData(data))
    })

    const DirectionsComponent = compose(
      withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBDJ7-qo34RvY_mdXfK-b-ImAm96S3tzUI",
        loadingElement: <div style={{ height: `400px` }} />,
        containerElement: <div style={{ width: `100%` }} />,
        mapElement: <div style={{ height: `600px`, width: `auto` }} />,
        origin: this.props.trackingLocationData.origin,
        waypoints,
        destination: this.props.trackingLocationData.destination
      }),
      withScriptjs,
      withGoogleMap,
      connect(null, mapDispatchToProps),
      lifecycle({
        componentDidMount() {
          const DirectionsService = new google.maps.DirectionsService();
          DirectionsService.route({
            origin: this.props.origin,
            destination: this.props.destination,
            waypoints: this.props.waypoints,
            optimizeWaypoints: true,
            travelMode: 'DRIVING'
          }, (result, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
              this.setState({
                directions: { ...result },
                markers: true
              })
              // add all return data to store including parsed distance from truck
              let legsArr = result.routes[0].legs
              const distance = legsArr.reduce((acc, leg) => acc + leg.distance.value, 1)
              this.props.addMapData({ ...result, distance })

            } else {
              console.error(`error fetching directions ${result}`);
            }
          });
        }
      })
    )(props =>
      <GoogleMap
        defaultZoom={3}
        defaultOptions={{
          styles: mapStyles
        }}
      ><Marker
          position={this.props.trackingLocationData.truckLocation}
          icon={{
            url: truckIcon,
            scaledSize: new google.maps.Size(50, 25)
          }}
        />
        {
          props.directions &&
          <DirectionsRenderer
            directions={props.directions}
            options={{
              suppressMarkers: true,
              polylineOptions: {
                strokeColor: "#7ddb46",
                strokeOpacity: 0.5,
                strokeWeight: 4
              }
            }}
          />}
      </GoogleMap>
    );

    return (
      <DirectionsComponent />
    )
  }
}

const mapStateToProps = (state) => ({
  trackingLocationData: state.trackingLocationData
})

export default connect(mapStateToProps)(WaypointsMap)
