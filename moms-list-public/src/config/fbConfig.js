import firebase from 'firebase/app'
import 'firebase/firestore' 
import 'firebase/auth' 

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCootd6-UXYcDjonCLKEbOgHhg28Cf4EBQ",
  authDomain: "ltr-app.firebaseapp.com",
  databaseURL: "https://ltr-app.firebaseio.com",
  projectId: "ltr-app",
  storageBucket: "ltr-app.appspot.com",
  messagingSenderId: "193724126473"
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true }); 

export default firebase;