import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import LinearProgress from '@material-ui/core/LinearProgress';

// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx"

import Style from "assets/jss/material-kit-pro-react/views/trucksPageStyle.jsx";

import LtrLogo from "assets/img/ltr/load-to-ride-logo.png"

import NavLinks from "components/Nav/NavLinks"

import LtrFreight from "assets/img/ltr/ltr-freight-background.jpg"
import Cascadia from "assets/img/ltr/freightliner-cascadia-truck.jpg"
import Mack from "assets/img/ltr/mack-truck.jpg"
import Tesla from "assets/img/ltr/tesla-semi.jpg"


// Sections for this page

import { mlAuto } from "../../assets/jss/material-kit-pro-react";
import { greenLight } from "../../assets/jss/ltr-styles";
import { green } from "@material-ui/core/colors";

const dashboardRoutes = [];

class TrucksPage extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
  }
  render() {
    const { classes, ...rest } = this.props;

    return (
      <div className={classes.wrapper}>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand="Load To Ride"
          links={<NavLinks dropdownHoverColor="dark" />}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: "dark"
          }}
          {...rest}
        />
        <Parallax image={require("assets/img/ltr/ltr-trucks-hd.JPG")} filter="dark">
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} md={8} lg={8} >
                <h1 className={classes.header1}>TRUCKS</h1>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classes.wrapper}>
          <br /><br /><br />
          <GridContainer justify="center">
            <GridItem xs={10}>
              <GridContainer spacing={40}>
                <GridItem xs={12} md={4}>
                  <Card
                    className={classes.cardStyles}
                  >
                    <CardBody>
                      <h1 className={classes.header1}>1,038+</h1>
                      <h4 className={classes.bodyText1}>Happy Clients</h4>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem xs={12} md={4}>
                  <Card
                    className={classes.cardStyles}
                  >
                    <CardBody>
                      <h1 className={classes.header1}>69,420+</h1>
                      <h4 className={classes.bodyText1}>Pallets Delivered</h4>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem xs={12} md={4}>
                  <Card
                    className={classes.cardStyles}
                  >
                    <CardBody>
                      <h1 className={classes.header1}>4,209,658+</h1>
                      <h4 className={classes.bodyText1}>Miles Driven</h4>
                    </CardBody>
                  </Card>
                </GridItem>
                <GridItem xs={12}>
                  <LinearProgress
                    variant="determinate"
                    value={69}
                    classes={{
                      barColorPrimary: classes.linearBarColorPrimary
                    }}
                  />
                  <GridContainer>
                    <GridItem xs={6}>
                      <h5 className={classes.bodyText1}>32 trucks on the road</h5>
                    </GridItem>
                    <GridItem xs={6}>
                      <h5 className={classes.bodyText1}>54 total trucks</h5>
                    </GridItem>
                  </GridContainer>
                </GridItem>
              </GridContainer>
              <br /><br /><br />
              <h2 className={classes.header1}>OUR GREEN INITIATIVE</h2>
              <h5 className={classes.bodyText1}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
              sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
              Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl
              ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate
              velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et
              accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore
              te feugait nulla facilisi.</h5>
              <br /><br /><br />
              <h2 className={classes.header1}>MEET THE FLEET</h2>
              <br /><br />
              <GridContainer spacing={40}>
                <GridItem xs={12} md={4}>
                  <img src={Cascadia} alt="Cascadia" className={classes.image} />
                </GridItem>
                <GridItem xs={12} md={8}>
                  <h3 className={classes.title}>Freightliner Cascadia</h3>
                  <p className={classes.bodyLeft}>With an aerodynamic exterior shaped to reduce drag and
                  improve fuel efficiency, the Cascadia is the best-selling class 8 truck in North America.
                  Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                  nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in
                  vulputate velit esse molestie consequat.</p>
                  <GridContainer spacing={32}>
                    <GridItem xs={6} s={3} md={3} lg={2} >
                      <Card className={classes.greenCard}>
                        <CardBody>
                          <p className={classes.blackBody}>Comprises</p>
                          <h4 className={classes.blackHeader}>43%</h4>
                          <p className={classes.blackBody}>of our fleets</p>
                        </CardBody>
                      </Card>
                    </GridItem>
                    <GridItem xs={6} s={3} md={3} lg={2} >
                      <Card className={classes.greenCard}>
                        <CardBody>
                          <p className={classes.blackBody}>Most Fuel Efficient</p>
                        </CardBody>
                      </Card>
                    </GridItem>
                    <GridItem xs={6} s={3} md={3} lg={2} >
                      <Card className={classes.greenCard}>
                        <CardBody>
                          <p className={classes.blackBody}>Superior Handling</p>
                        </CardBody>
                      </Card>
                    </GridItem>
                  </GridContainer>
                </GridItem>
                <GridItem xs={12} md={4}>
                  <img src={Mack} alt="Mack Truck" className={classes.image} />
                </GridItem>
                <GridItem xs={12} md={8}>
                  <h3 className={classes.title}>Mack Anthem</h3>
                  <p className={classes.bodyLeft}>Born of the American spirit, the new Mack Anthem™ comes standard with a bold 
                  design that delivers serious aerodynamics and a new interior that keeps drivers comfortable and productive. 
                  Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex 
                  ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
                  consequat.</p>
                  <GridContainer spacing={32}>
                    <GridItem xs={6} s={3} md={3} lg={2} >
                      <Card className={classes.greenCard}>
                        <CardBody>
                          <p className={classes.blackBody}>Comprises</p>
                          <h4 className={classes.blackHeader}>31%</h4>
                          <p className={classes.blackBody}>of our fleets</p>
                        </CardBody>
                      </Card>
                    </GridItem>
                    <GridItem xs={6} s={3} md={3} lg={2} >
                      <Card className={classes.greenCard}>
                        <CardBody>
                          <p className={classes.blackBody}>Easiets To Drive</p>
                        </CardBody>
                      </Card>
                    </GridItem>
                    <GridItem xs={6} s={3} md={3} lg={2} >
                      <Card className={classes.greenCard}>
                        <CardBody>
                          <p className={classes.blackBody}>Great Turn Capability</p>
                        </CardBody>
                      </Card>
                    </GridItem>
                  </GridContainer>
                </GridItem>
                <GridItem xs={12} md={4}>
                  <img src={Tesla} alt="Tesla Semi" className={classes.image} />
                </GridItem>
                <GridItem xs={12} md={8}>
                  <h3 className={classes.title}>Tesla Semi</h3>
                  <p className={classes.bodyLeft}>Semi is the safest, most comfortable truck ever. Four independent motors 
                  provide maximum power and acceleration and require the lowest energy cost per mile. Ut wisi enim ad minim 
                  veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
                  Duis autem vel eum iriure dolor in hendrerit in vulputate vlit esse molestie consequat.</p>
                  <GridContainer spacing={32}>
                    <GridItem xs={6} s={3} md={3} lg={2} >
                      <Card className={classes.greenCard}>
                        <CardBody>
                          <p className={classes.blackBody}>Comprises</p>
                          <h4 className={classes.blackHeader}>26%</h4>
                          <p className={classes.blackBody}>of our fleets</p>
                        </CardBody>
                      </Card>
                    </GridItem>
                    <GridItem xs={6} s={3} md={3} lg={2} >
                      <Card className={classes.greenCard}>
                        <CardBody>
                          <p className={classes.blackBody}>Fastest Accelleration</p>
                        </CardBody>
                      </Card>
                    </GridItem>
                    <GridItem xs={6} s={3} md={3} lg={2} >
                      <Card className={classes.greenCard}>
                        <CardBody>
                          <p className={classes.blackBody}>Impressive Braking</p>
                        </CardBody>
                      </Card>
                    </GridItem>
                  </GridContainer>
                </GridItem>
              </GridContainer>
            </GridItem>
          </GridContainer>

          <br /><br /><br /><br />
          <div
            style={{
              background: "url(" + LtrFreight + ")",
              backgroundSize: "cover",
              height: "60vh"
            }}
          >
            <div className={classes.container}>
            <br /><br /><br /><br /><br /><br /><br />
              <h1 className={classes.header1}>GET A QUOTE</h1>
              <br />
              <Button
                className={classes.ltrButton}
                size="lg"
                href="/estimator"                
                rel="noopener noreferrer"
              >
                PRICE ESTIMATOR
            </Button>
            </div>
          </div>
          <br /><br /><br /><br /><br />
          <div
            className={classes.footerContainer}
          >
            <br /><br />
            <Footer
              content={
                <div>
                  <div className={classes.pullCenter}>
                    <a
                      href="/"
                      className={classes.footerBrand}
                    >
                      <img
                        src={LtrLogo}
                        alt="Load To Ride Logo"
                        style={{
                          height: "auto",
                          width: "10%"
                        }}
                      />
                    </a>
                  </div>
                  <NavLinks />
                  <br />
                  <br />
                </div>
              }
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(Style)(TrucksPage);
