import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx"

import Style from "assets/jss/material-kit-pro-react/views/tracking/trackingPageStyle.jsx";

import LtrLogo from "assets/img/ltr/load-to-ride-logo.png"

import NavLinks from "components/Nav/NavLinks"

// Sections for this page
import TrackingForm from "components/Forms/TrackingForm.jsx"
import CustomerServiceForm from "components/Forms/CustomerServiceForm.jsx"
import { mlAuto } from "../../assets/jss/material-kit-pro-react";

const dashboardRoutes = [];

class TrackingPage extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
  }
  render() {
    const { classes, ...rest } = this.props;

    return (
      <div className={classes.wrapper}>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand="Load To Ride"
          links={<NavLinks dropdownHoverColor="dark" />}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: "dark"
          }}
          {...rest}
        />
        <Parallax image={require("assets/img/ltr/load-to-ride-tracking-hd.JPG")} filter="dark">
          <div className={classes.container}>
            <GridContainer
              justify="center"
            >
              <GridItem xs={12} md={8} lg={8} >
                <h1 className={classes.header1}>TRACK YOUR SHIPMENT</h1>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classes.wrapper}>
          <GridContainer justify="center">
            <GridItem xs={12} md={8} >
              <TrackingForm />
            </GridItem>
          </GridContainer>
          <br /><br /><br /><br />
          <CustomerServiceForm />          
          <br /><br /><br /><br /><br />
          <div
            className={classes.footerContainer}
          >
            <br /><br />
            <Footer
              content={
                <div>
                  <div className={classes.pullCenter}>
                    <a
                      href="/"
                      className={classes.footerBrand}
                    >
                      <img
                        src={LtrLogo}
                        alt="Load To Ride Logo"
                        style={{
                          height: "auto",
                          width: "10%"
                        }}
                      />
                    </a>
                  </div>
                  <NavLinks />
                  <br />
                  <br />
                </div>
              }
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(Style)(TrackingPage);
