import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import LinearProgress from '@material-ui/core/LinearProgress';

// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";
import Place from "@material-ui/icons/Place";
import LocalPostOffice from "@material-ui/icons/LocalPostOffice"
import Phone from "@material-ui/icons/Phone"

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx"
import InfoArea from "components/InfoArea/InfoArea.jsx";

import Style from "assets/jss/material-kit-pro-react/views/contactPageStyle.jsx";

import LtrLogo from "assets/img/ltr/load-to-ride-logo.png"
import LtrLocation from "assets/img/ltr/ltrLocation.jpg"

import NavLinks from "components/Nav/NavLinks"

import Resume from "assets/img/ltr/ltrResume.jpg"

// Sections for this page

import ContactForm from "components/Forms/ContactForm.jsx"

import { mlAuto } from "../../assets/jss/material-kit-pro-react";
import { greenLight } from "../../assets/jss/ltr-styles";
import { green } from "@material-ui/core/colors";

const dashboardRoutes = [];

class EstimatorPage extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
  }
  render() {
    const { classes, ...rest } = this.props;

    return (
      <div className={classes.wrapper}>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand="Load To Ride"
          links={<NavLinks dropdownHoverColor="dark" />}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: "dark"
          }}
          {...rest}
        />
        <Parallax image={require("assets/img/ltr/load-to-ride-contact-hd.JPG")} filter="dark">
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} md={8} lg={8} >
                <h1 className={classes.header1}>CONTACT</h1>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classes.wrapper}>
          <br /><br /><br />
          <GridContainer justify="center">
            <GridItem xs={12} s={10} md={8}>
              <h5 className={classes.bodyText1}>Load to Ride is your partner in transportation. We are here to help.
              Contact us for rates, general shipment questions or just to say hi! Fill out our contact form and one of
              our experts will be in touch shortly.</h5>
              <br /><br />
              <h2 className={classes.header1}>OFFICE HOURS</h2>
            </GridItem>
          </GridContainer>
          <br /><br /><br />
          <GridContainer justify="center">
            <GridItem xs={12} s={10}>
              <GridContainer justify="center">
                <GridItem xs={12} s={6} md={1}>
                  <div className={classes.cardContainer}>
                    <div className={classes.cardHeader}>
                      <h4 className={classes.cardTitle}>SUNDAY</h4>
                    </div>
                    <p className={classes.cardText}>CLOSED</p>
                  </div>
                </GridItem>
                <GridItem xs={12} s={6} md={1}>
                  <div className={classes.cardContainer}>
                    <div className={classes.cardHeader}>
                      <h4 className={classes.cardTitle}>MONDAY</h4>
                    </div>
                    <p className={classes.cardText}>9AM - 5PM</p>
                  </div>
                </GridItem>
                <GridItem xs={12} s={6} md={1}>
                  <div className={classes.cardContainer}>
                    <div className={classes.cardHeader}>
                      <h4 className={classes.cardTitle}>TUESDAY</h4>
                    </div>
                    <p className={classes.cardText}>9AM - 5PM</p>
                  </div>
                </GridItem>
                <GridItem xs={12} s={6} md={1}>
                  <div className={classes.cardContainer}>
                    <div className={classes.cardHeader}>
                      <h4 className={classes.cardTitle}>WEDNESDAY</h4>
                    </div>
                    <p className={classes.cardText}>9AM - 5PM</p>
                  </div>
                </GridItem>
                <GridItem xs={12} s={6} md={1}>
                  <div className={classes.cardContainer}>
                    <div className={classes.cardHeader}>
                      <h4 className={classes.cardTitle}>THURSDAY</h4>
                    </div>
                    <p className={classes.cardText}>9AM - 5PM</p>
                  </div>
                </GridItem>
                <GridItem xs={12} s={6} md={1}>
                  <div className={classes.cardContainer}>
                    <div className={classes.cardHeader}>
                      <h4 className={classes.cardTitle}>FRIDAY</h4>
                    </div>
                    <p className={classes.cardText}>9AM - 5PM</p>
                  </div>
                </GridItem>
                <GridItem xs={12} s={6} md={1}>
                  <div className={classes.cardContainer}>
                    <div className={classes.cardHeader}>
                      <h4 className={classes.cardTitle}>SATURDAY</h4>
                    </div>
                    <p className={classes.cardText}>CLOSED</p>
                  </div>
                </GridItem>
              </GridContainer>
            </GridItem>
          </GridContainer>
          <br /> <br /> <br /> <br /> <br />
          <img src={LtrLocation} alt="LTR Location" />
          <br /> <br /> <br /> <br /> <br />
          <GridContainer>
            <GridItem xs={12} md={6}>
              <br /><br />
              <GridContainer justify="center">
                <GridItem xs={12} s={10} md={8}>
                  <h2 className={classes.headerLeft}>FEEL FREE TO REACH OUT</h2>
                  <hr className={classes.hr} />
                  <GridContainer>
                    <GridItem xs={1}>
                      <Place className={classes.icon} />
                    </GridItem>
                    <GridItem xs={11}>
                      <p className={classes.bodyLeft}>
                        3680 East 52nd Avenue <br />
                        Denver, CO 80216
                      </p>
                    </GridItem>
                    <br />
                    <GridItem xs={1}>
                      <LocalPostOffice className={classes.icon} />
                    </GridItem>
                    <GridItem xs={11}>
                      <p className={classes.bodyLeft}>
                        custsvcs@ltrtransportation.com
                      </p>
                    </GridItem>
                    <br />
                    <GridItem xs={1}>
                      <Phone className={classes.icon} />
                    </GridItem>
                    <GridItem xs={11}>
                      <p className={classes.bodyLeft}>
                        +1 (720) 277-0030
                      </p>
                    </GridItem>
                  </GridContainer>
                </GridItem>
              </GridContainer>
            </GridItem>
            <GridItem xs={12} md={6}>
              <ContactForm />
            </GridItem>
          </GridContainer>
          <br /> <br /> <br /> <br /> <br />
          <GridContainer justify="center">
            <GridItem xs={12} sm={10}>
              <h2 className={classes.header1}>FREQUENTLY ASKED QUESTIONS</h2>
              <br />
              <Card
                className={classes.cardStyles}
              >               
                  <h4 className={classes.bodyLeft}>This is a frequently asked question. Can you resolve it?</h4>                
              </Card>
              <br/>
              <Card
                className={classes.cardStyles}
              >               
                  <h4 className={classes.bodyLeft}>This is a frequently asked question. Can you resolve it?</h4>                
              </Card>
              <br/>
              <Card
                className={classes.cardStyles}
              >               
                  <h4 className={classes.bodyLeft}>This is a frequently asked question. Can you resolve it?</h4>                
              </Card>
            </GridItem>
          </GridContainer>
          <br /> <br /> <br /> <br /> <br />
          <div
            className={classes.footerContainer}
          >
            <br /><br />
            <Footer
              content={
                <div>
                  <div className={classes.pullCenter}>
                    <a
                      href="/"
                      className={classes.footerBrand}
                    >
                      <img
                        src={LtrLogo}
                        alt="Load To Ride Logo"
                        style={{
                          height: "auto",
                          width: "10%"
                        }}
                      />
                    </a>
                  </div>
                  <NavLinks />
                  <br />
                  <br />
                </div>
              }
            />
          </div>
        </div >
      </div >
    );
  }
}

export default withStyles(Style)(EstimatorPage);
