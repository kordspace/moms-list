import React, { Component } from 'react'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import { Link } from 'react-router-dom'
import { createProject } from '../../store/actions/projectActions'
import Collapsible from 'react-collapsible';
import StripeCheckout from 'react-stripe-checkout';

class Memberships extends Component {
    onToken = (token) => {
        fetch('/save-stripe-token', {
          method: 'POST',
          body: JSON.stringify(token),
        });
      }

    render(){
        const { guestMembershipsPage } = this.props;

        if (guestMembershipsPage) {
        return (
            <div className=" container-fluid imagecover" id="member-img">
                <div className="row container white-text" id="grow-banner">
                     <p className="grow-hero"><br></br><br></br>{guestMembershipsPage[1].header}</p><br></br><br></br><br></br>
                </div>
                <div className="teal2 lighten-2 white-text grow-role">
                    <p className="container" id="purposetealtext">{guestMembershipsPage[4].content1}</p>
                </div>
                <div className="white black-text purposebeliefssub">
                    <div className="container">
                        <p className="membershipheader">
                        <span className="bold">{guestMembershipsPage[0].header}</span> {guestMembershipsPage[0].subheader}
                        </p>
                        <p className="">              
                            {guestMembershipsPage[0].content1}                        
                        </p>
                        <p className="">
                            {guestMembershipsPage[0].content2}    
                        </p>
                        <Collapsible trigger="Benefits                        v">
                                    <p>{guestMembershipsPage[0].dropdown}    </p>
                        </Collapsible> 
                        <StripeCheckout
                            amount="30000"
                            billingAddress
                            description="Corporate Membership"
                            image="https://yourdomain.tld/images/logo.svg"
                            locale="auto"
                            name="FFPO"
                            stripeKey="pk_live_2OdkdNdiEr7jwG2wrD5Lht3c"
                            token={this.onToken}
                            zipCode
                        />
                        <p className="membershipheader">
                        <span className="bold">{guestMembershipsPage[2].header}</span> {guestMembershipsPage[2].subheader}
                        </p>
                        <p className="">              
                        {guestMembershipsPage[2].content1}
                        </p>
                        <p className="">
                        {guestMembershipsPage[2].content2}
                        </p>
                        <Collapsible trigger="Benefits                        v">
                            <p>{guestMembershipsPage[2].dropdown}</p>
                        </Collapsible> 
                        <StripeCheckout
                            amount="20000"
                            billingAddress
                            description="Individual Membership"
                            image="https://yourdomain.tld/images/logo.svg"
                            locale="auto"
                            name="FFPO"
                            stripeKey="pk_live_2OdkdNdiEr7jwG2wrD5Lht3c"
                            token={this.onToken}
                            zipCode
                        />
                        <div className="center">
                            <a href='/memberships' target="_blank"><button className="btn btn-large teal2 z-depth-0 memberbtn">{guestMembershipsPage[3].header}</button></a>
                        </div>
                        <StripeCheckout
                            amount="100"
                            billingAddress
                            description="Corporate Membership"
                            image="https://yourdomain.tld/images/logo.svg"
                            locale="auto"
                            name="FFPO"
                            stripeKey="pk_live_2OdkdNdiEr7jwG2wrD5Lht3c"
                            token={this.onToken}
                            lavel="Test"
                            zipCode
                        />

                    </div>
                </div>
            </div>
        )
        } else {
            return (
                <div></div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    console.log(state);
    return {
        guestMembershipsPage: state.firestore.ordered.guestMembershipsPage,
        auth: state.firebase.auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createProject: (project) => dispatch(createProject(project))
    }
}

//grab assessment data from firestore in firebase where the assessment matches the logged in user and only show one latest result
export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect(props => [
        {
          collection: 'guestMembershipsPage',
        },
      ])
      
)(Memberships)