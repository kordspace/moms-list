import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
import Subject from "@material-ui/icons/Subject";
import WatchLater from "@material-ui/icons/WatchLater";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx"
import CardBody from "components/Card/CardBody.jsx";

import benefitsStyle from "assets/jss/material-kit-pro-react/views/MainPage/benefitsStyle.jsx";

//Photos
import LtrSemi from "assets/img/ltr/ltr-sem-truck-phillips.jpg"
import LtrFreight from "assets/img/ltr/ltr-freight.jpg"
import LtrOffice from "assets/img/ltr/load-to-ride-office.jpg"


class SectionBenefits extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={10} md={8}>
            <h5 className={classes.quote}>
              "It is a great feeling to know that our freight does not get handled until it reaches my customers. Load to Ride Transportation does a great job minimizing the handling of our products."
            </h5>
            <h2 className={classes.title}>-LELAND ROSS</h2>
            <h5 className={classes.subtitle}>ROSS COMPANY CORPORATION</h5>
          </GridItem>
        </GridContainer>
        <br />
        <br />
        <GridContainer spacing={8}>
          <GridItem xs={12} sm={12} md={4}>
            <Card
              background
              className={classes.cardStyles}
              style={{
                backgroundImage: "url(" + LtrSemi + ")"
              }}
            >
              <CardBody background>
                <h2 className={classes.cardTitle}>SUPERIOR <br />TECHNOLOGY</h2>
                <h5 className={classes.description}>
                  We provide a complete range of technology-based applications that help speed the flow
                  of information through your supply pipeline. We give our clients the tools they need
                  to be successful. From tracking and rates to document retrieval and downloadable reports
                  we provide easy access to needed information, anytime, anywhere.
                </h5>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem            
            xs={12} sm={12} md={4}
          >
            <Card
              background
              className={classes.cardStyles}
              style={{
                backgroundImage: "url(" + LtrOffice + ")"
              }}
            >
              <CardBody background>
                <h2 className={classes.cardTitle}>BEST IN HOUSE <br />SOLUTION</h2>
                <h5 className={classes.description}>
                  Our highly specialized operations team and account-specific customer service organization
                  provides the best support to build long-lasting business relationships. From a single delivery
                  van to our truck fleet, Load to Ride provides clients with the finest, most dependable assets and
                  experts to move your freight.                  
                </h5>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Card
              background
              className={classes.cardStyles}
              style={{
                backgroundImage: "url(" + LtrFreight + ")"
              }}
            >
              <CardBody background>
                <h2 className={classes.cardTitle}>A DIFFERENT <br />WAY TO SHIP</h2>
                <h5 className={classes.description}>
                  We have developed an innovative network and business model that enables us to optimize service levels
                  and provides better rates and service to our customers.
            </h5>
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(benefitsStyle)(SectionBenefits);
