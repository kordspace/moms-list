import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
import Subject from "@material-ui/icons/Subject";
import WatchLater from "@material-ui/icons/WatchLater";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx"
import CardBody from "components/Card/CardBody.jsx";
import Button from "components/CustomButtons/Button.jsx";

import servicesStyle from "assets/jss/material-kit-pro-react/views/MainPage/servicesStyle.jsx";

//Photos
import LtrSemi from "assets/img/ltr/ltr-sem-truck-phillips.jpg"
import LtrFreight from "assets/img/ltr/ltr-freight.jpg"
import LtrOffice from "assets/img/ltr/load-to-ride-office.jpg"


class SectionServices extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={10} md={10}>
            <h1 className={classes.title}>SERVICES</h1>
          </GridItem>
        </GridContainer>
        <br />
        <br />
        <GridContainer spacing={8}>
          <GridItem xs={12} sm={6} md={3}>
            <Card
              className={classes.cardStyles}
            >
              <CardBody>
                <h2 className={classes.cardTitle}>LOAD TO DELIVER, LTL<br />& VOLUME SHIPMENTS</h2>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card
              className={classes.cardStyles}
            >
              <CardBody>
                <h2 className={classes.cardTitle}>POOL DISTRIBUTION</h2>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card
              className={classes.cardStyles}
            >
              <CardBody>
                <h2 className={classes.cardTitle}>WAREHOUSING</h2>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card
              className={classes.cardStyles}
            >
              <CardBody>
                <h2 className={classes.cardTitle}>EXPEDITED SERVICES</h2>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card
              className={classes.cardStyles}
            >
              <CardBody>
                <h2 className={classes.cardTitle}>SPECIAL HANDLING</h2>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card
              className={classes.cardStyles}
            >
              <CardBody>
                <h2 className={classes.cardTitle}>HAZMAT CERTIFIED</h2>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card
              className={classes.cardStyles}
            >
              <CardBody>
                <h2 className={classes.cardTitle}>TEMPERATURE<br />PROTECTION</h2>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem
            xs={12} sm={6} md={3}
            className={classes.gridItem}
          >
            <Button
                  className={classes.ltrButton}
                  size="lg"
                  href="/services"                  
                  rel="noopener noreferrer"
                >
              LEARN MORE
              </Button>

          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(servicesStyle)(SectionServices);
