import React from "react";
import { Link } from 'react-router-dom'

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
// @material-ui/icons
import Search from "@material-ui/icons/Search";
import Email from "@material-ui/icons/Email";
import Face from "@material-ui/icons/Face";
import Settings from "@material-ui/icons/Settings";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Explore from "@material-ui/icons/Explore";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Header from "components/Header/Header.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import CustomDropdown from "components/CustomDropdown/CustomDropdown.jsx";
import Button from "components/CustomButtons/Button.jsx";

import navbarsStyle from "assets/jss/material-kit-pro-react/views/componentsSections/navbarsStyle.jsx";

import image from "assets/img/bg.jpg";
import profileImage from "assets/img/faces/avatar.jpg";

class SectionNavbars extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Header
        fixed
        brand="Navbar with profile"
        links={
          <List className={classes.list + " " + classes.mlAuto}>
            <ListItem className={classes.listItem}>
              <Button
                href="/"
                className={classes.navLink}
                color="transparent"
              >
                <div style={{ fontWeight: `900`, fontSize: '.8rem' }}>HOME</div>
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="/"
                className={classes.navLink}
                color="transparent"
              >
                <div style={{ fontWeight: `900`, fontSize: '.8rem' }}>ABOUT</div>
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="/services"
                className={classes.navLink}
                color="transparent"
              >
                <div style={{ fontWeight: `900`, fontSize: '.8rem' }}>SERVICES</div>
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="/"
                className={classes.navLink}
                color="transparent"
              >
                <div style={{ fontWeight: `900`, fontSize: '.8rem' }}>ORDERS</div>
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="/"
                className={classes.navLink}
                color="transparent"
              >
                <div style={{ fontWeight: `900`, fontSize: '.8rem' }}>TRACK</div>
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="/"
                className={classes.navLink}
                color="transparent"
              >
                <div style={{ fontWeight: `900`, fontSize: '.8rem' }}>TRUCKS</div>
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="/"
                className={classes.navLink}
                color="transparent"
              >
                <div style={{ fontWeight: `900`, fontSize: '.8rem' }}>JOBS</div>
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="/"
                className={classes.navLink}
                color="transparent"
              >
                <div style={{ fontWeight: `900`, fontSize: '.8rem' }}>ESTIMATOR</div>
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="/"
                className={classes.navLink}
                color="transparent"
              >
                <div style={{ fontWeight: `900`, fontSize: '.8rem' }}>CONTACT</div>
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="/"
                className={classes.navLink}
                color="transparent"
              >
                <div style={{ fontWeight: `900`, fontSize: '.8rem' }}>LOGIN</div>
              </Button>
            </ListItem>
            
            {/* <ListItem className={classes.listItem}> FOR DROPDOWN NAV ITEMS
              <CustomDropdown
                buttonText="LEARN"
                buttonProps={{
                  className: classes.navLink,
                  color: "transparent",
                }}
                dropPlacement="bottom"
                dropdownList={[
                  { href: "/purposefulleadership", content: "What is Purposeful Leadership?" },
                  { href: "/practices", content: "The Practices of Purposeful Leadership" },
                  { href: "/whypurpose", content: "Why Purpose?" },
                ]}
              />
            </ListItem> */}            
          </List>
        }
      />
    );
  }
}

export default withStyles(navbarsStyle)(SectionNavbars);
