import React from "react";
// react component for creating beautiful carousel
import Carousel from "react-slick";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
import LocationOn from "@material-ui/icons/LocationOn";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import carouselStyle from "assets/jss/material-kit-pro-react/views/componentsSections/carouselStyle.jsx";
import image1 from "assets/img/bg.jpg";
import image2 from "assets/img/bg2.jpg";
import image3 from "assets/img/bg3.jpg";
import Button from "components/CustomButtons/Button.jsx";


class SectionCarousel extends React.Component {
  render() {
    const { classes } = this.props;
    const settings = {
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true
    };
    return (
      <div  id="carousel">
        <div >
          <GridContainer>
            <GridItem xs={12} sm={12} md={12} >
                <Carousel {...settings} className={classes.hero}>
                  <div className={classes.content}>
                    <img
                      src={image1}
                      alt="First slide"
                      className="slick-image"
                    />
                    <h1>Product Manufacturers</h1>
                    <Button
                        onClick={e => {
                          e.preventDefault();
                          this.smoothScroll("about");
                        }}
                        color="info"
                        className={classes.navButton}
                        round
                      >
                        OFFSET YOUR GLASS FOOTPRINT 
                      </Button>

                    {/*<div className="slick-caption">
                      <h4>
                        <LocationOn className="slick-icons" />Yellowstone
                        National Park, United States
                      </h4>
    </div>*/}
                  </div>
                  <div className={classes.content}>
                    <img
                      src={image2}
                      alt="First slide"
                      className="slick-image"
                    />
                    <h1>Conscious consumers (and CEOs)</h1>
                    <Button
                        onClick={e => {
                          e.preventDefault();
                          this.smoothScroll("consumers");
                        }}
                        color="info"
                        className={classes.navButton}
                        round
                      >
                        OFFSET YOUR GLASS FOOTPRINT 
                      </Button>

                    {/*<div className="slick-caption">
                      <h4>
                        <LocationOn className="slick-icons" />Yellowstone
                        National Park, United States
                      </h4>
    </div>*/}
                  </div>
                  <div className={classes.content}>
                    <img
                      src={image3}
                      alt="First slide"
                      className="slick-image"
                    />
                    <h1>Waste Management & Recycling Industry</h1>
                    <Button
                        onClick={e => {
                          e.preventDefault();
                          this.smoothScroll("suppliers");
                        }}
                        color="info"
                        className={classes.navButton}
                        round
                      >
                        JOIN THE MOVEMENT 
                      </Button>

                    {/*<div className="slick-caption">
                      <h4>
                        <LocationOn className="slick-icons" />Yellowstone
                        National Park, United States
                      </h4>
    </div>*/}
                  </div>
                  <div>
                    <img
                      src={image3}
                      alt="Third slide"
                      className="slick-image"
                    />
                    <h1>Waste Management & Recycling Industry</h1>
                    <Button
                        onClick={e => {
                          e.preventDefault();
                          this.smoothScroll("about");
                        }}
                        color="info"
                        className={classes.navButton}
                        round
                      >
                         PLAY A PART WHILE MAKING MORE MONEY COVERING NEGATIVE COSTS
                      </Button>

                    <div className="slick-caption">
                      <h4>
                        <LocationOn className="slick-icons" />Yellowstone
                        National Park, United States
                      </h4>
                    </div>
                  </div>
                </Carousel>
            </GridItem>
          </GridContainer>
        </div>
      </div>
    );
  }
}

export default withStyles(carouselStyle)(SectionCarousel);
