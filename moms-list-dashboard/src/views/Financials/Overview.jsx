import React from "react";
import { connect } from 'react-redux';
import compose from 'recompose/compose';

// react component for creating Charts
import ChartistGraph from "react-chartist";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Paper from '@material-ui/core/Paper';
// @material-ui/icons
import Person from "@material-ui/icons/Person";
import Edit from "@material-ui/icons/Edit";
import Assignment from "@material-ui/icons/Assignment";
import Dvr from "@material-ui/icons/Dvr";
import Favorite from "@material-ui/icons/Favorite";
import Close from "@material-ui/icons/Close";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import ArrowDownward from "@material-ui/icons/ArrowDownward";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Table from "components/Table/Table.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import { cardTitle } from "assets/jss/material-dashboard-pro-react.jsx";
import Style from "assets/jss/material-dashboard-pro-react/views/financials/overviewStyle";

const addButtons = (dataArr, buttons) => {
  let acc = []
  for (var i = 0; i < dataArr.length; i++) {
    if (dataArr[i].length < 4) {
      dataArr[i].push(buttons)
    }
    acc.push(dataArr[i])
  }
  return acc
}

class Overview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: this.props.tasks
    }
  }
  render() {
    const { classes, expenses, estimates, sales } = this.props;

    const simpleButtons = [
      { color: "success", icon: Edit },
      { color: "danger", icon: Close }
    ].map((prop, key) => {
      return (
        <Button
          color={prop.color}
          simple
          className={classes.actionButton}
          key={key}
        >
          <prop.icon className={classes.icon} />
        </Button>
      );
    });
    let tableData = addButtons(this.state.tasks.tableData, simpleButtons)

    return (
      <div>
        <GridContainer>
          <GridItem xs={6} sm={2}>
            <Card>
              <h2 className={classes.cardTitle}><ArrowUpward className={classes.successIcon} />$3,046</h2>
              <p className={classes.cardSubtitle}>Revenue / Truck</p>
            </Card>
          </GridItem>
          <GridItem xs={6} sm={2}>
            <Card>
              <h2 className={classes.cardTitle}><ArrowDownward className={classes.dangerIcon} />0.019</h2>
              <p className={classes.cardSubtitle}>Drivers / Overhead</p>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={8}>
            <Card>
              <div className={classes.alignRight}><h1 className={classes.cardTitle}>$40,634/12,000,000</h1></div>
            </Card>
          </GridItem>
        </GridContainer>
        <GridContainer>
          <GridItem xs={12}>
            <br /><br />
            <h1 className={classes.cardTitle}>INCOME</h1>
          </GridItem>
          <GridItem xs={12} sm={4}>
            <Card>
              <h2 className={classes.cardTitle}>$5,046</h2>
              <p className={classes.cardSubtitle}>OPEN INVOICES</p>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={4}>
            <Card>
              <h2 className={classes.cardTitle}>$2,151</h2>
              <p className={classes.cardSubtitleDanger}>OVERDUE</p>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={4}>
            <Card>
              <h2 className={classes.cardTitle}>$3,551</h2>
              <p className={classes.cardSubtitleSuccess}>PAID (LAST 30 DAYS)</p>
            </Card>
          </GridItem>
        </GridContainer>
        <GridContainer>
          <GridItem xs={12} sm={6}>
            <br /><br />
            <Card>
              <h1 className={classes.cardTitle}>EXPENSES</h1>
              <GridContainer alignItems="center">
                <GridItem xs={12} sm={4} >
                  <h1 className={classes.cardTitle}>$5,206</h1>
                </GridItem>
                <GridItem xs={12} sm={8}>
                  <ChartistGraph
                    data={expenses.data}
                    type="Pie"
                    options={expenses.options}
                  />
                </GridItem>
              </GridContainer>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6}>
            <br /><br />
            <Card>
              <h1 className={classes.cardTitle}>NEW BUSINESS ESTIMATES</h1>
              <ChartistGraph
                data={estimates.data}
                type="Line"
                options={estimates.options}
              />
            </Card>
          </GridItem>
        </GridContainer>
        <GridContainer>
          <GridItem xs={12} sm={6}>
            <Card>
              <h1 className={classes.cardTitle}>TASKS</h1>
              <Table
                tableHead={this.state.tasks.tableHead}
                tableData={tableData}
                tableHeaderColor='white'
              />
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6}>
            <Card>
              <h1 className={classes.cardTitle}>MONTHLY SALES</h1>
              <ChartistGraph
                data={sales.data}
                type="Line"
                options={sales.options}
              />
            </Card>
          </GridItem>
        </GridContainer>
      </div>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    expenses: state.financials.expenses,
    estimates: state.financials.estimates,
    sales: state.financials.sales,
    tasks: state.financials.tasks,
  }
}

export default compose(
  connect(mapStateToProps),
  withStyles(Style)
)(Overview);
