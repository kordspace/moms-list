const initState = {
  authError: null,
}

export default (state = initState, action) => {
  switch (action.type) {
    case 'LOGIN':
      console.log('LOGIN')
      return {
        ...state,
        user: action.user
      }
    case 'LOGIN_ERROR':
      console.log('LOGIN_ERROR')
      return {
        ...state,
        authError: 'LOGIN FAILED, please check your username and password.'
      }
    case 'LOGOUT':
      console.log('LOGOUT')
      return {}     
    default:
      return state
  }
}
