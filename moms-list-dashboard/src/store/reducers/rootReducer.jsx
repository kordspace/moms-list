import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import authReducer from './authReducer'
import brokeringReducer from './brokeringReducer'
import customersReducer from './customersReducer'
import financialsReducer from './financialsReducer';

export default (history) => combineReducers({
  router: connectRouter(history),
  auth: authReducer,
  brokering: brokeringReducer,
  customers: customersReducer,
  financials: financialsReducer
});

