// Orders Data
const openOrders = {
  columns: [
    {
      Header: "REPRESENTATIVE",
      accessor: "rep"
    },
    {
      Header: "ORGONIZATION",
      accessor: "org"
    },
    {
      Header: "PHONE",
      accessor: "phone"
    },
    {
      Header: "EMAIL",
      accessor: "email"
    },
    {
      Header: "PALLET CT",
      accessor: "pallets"
    },
    {
      Header: "WEIGHT",
      accessor: "weight"
    },
    {
      Header: "PICKUP ZIP",
      accessor: "zip"
    },
    {
      Header: "BALANCE",
      accessor: "balance"
    },
    {
      Header: "STATUS",
      accessor: "status"
    },
    {
      Header: "",
      accessor: "actions",
      sortable: false,
      filterable: false
    }
  ],
  dataRows: [
    ["Steven Ball","Ball Aerospace","555-555-5555", "sball@ballaero.com", "40 pallets", "500lbs", "80302", "$1,067.59", "In Transit"],
    ["Steven Ball","Ball Aerospace","555-555-5555", "sball@ballaero.com", "40 pallets", "500lbs", "80302", "$1,067.59", "In Transit"],
    ["Steven Ball","Ball Aerospace","555-555-5555", "sball@ballaero.com", "40 pallets", "500lbs", "80302", "$1,067.59", "In Transit"]       
  ]
};

const incompleteOrders = {
  columns: [
    {
      Header: "REPRESENTATIVE",
      accessor: "rep"
    },
    {
      Header: "ORGONIZATION",
      accessor: "org"
    },
    {
      Header: "PHONE",
      accessor: "phone"
    },
    {
      Header: "EMAIL",
      accessor: "email"
    },
    {
      Header: "PALLET CT",
      accessor: "pallets"
    },
    {
      Header: "WEIGHT",
      accessor: "weight"
    },
    {
      Header: "PICKUP ZIP",
      accessor: "zip"
    },
    {
      Header: "BALANCE",
      accessor: "balance"
    },
    {
      Header: "STATUS",
      accessor: "status"
    },
    {
      Header: "",
      accessor: "actions",
      sortable: false,
      filterable: false
    }
  ],
  dataRows: [
    ["Steven Ball","Ball Aerospace","?", "sball@ballaero.com", "40 pallets", "500lbs", "80302", "$1,067.59", "Incomplete"],
    ["?","Ball Aerospace","555-555-5555", "?", "40 pallets", "?", "?", "$1,067.59", "In Transit"]      
  ]
};

const initState = {
  openOrders,
  incompleteOrders
}

export default (state = initState, action) => {
  switch (action.type) {
    default:
      return state
  }
}
