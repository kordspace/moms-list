import Charts from "views/Charts/Charts.jsx";

import Orders from "views/Brokering/Orders.jsx";
import Brokers from "views/Brokering/Brokers.jsx";
import History from "views/Brokering/History.jsx";
import Customers from "../views/Customers/Customers.jsx";
import Overview from "views/Financials/Overview.jsx"
import Invoices from "views/Financials/Invoices.jsx"
import Employees from "views/Financials/Employees.jsx"
import Dispatch from "views/Dispatch/Dispatch.jsx"

import RTLSupport from "views/Pages/RTLSupport.jsx";
import TimelinePage from "views/Pages/Timeline.jsx";
import UserProfile from "views/Pages/UserProfile.jsx";

import pagesRoutes from "./pages.jsx";

// @material-ui/icons
import DashboardIcon from "@material-ui/icons/Dashboard";
import Apps from "@material-ui/icons/Apps";
// import ContentPaste from "@material-ui/icons/ContentPaste";

import Place from "@material-ui/icons/Place";
import Timeline from "@material-ui/icons/Timeline";



export default [
  {
    collapse: true,
    path: "/brokering",
    name: "Brokering",
    icon: DashboardIcon,
    state: "openBrokering",
    views: [
      { path: "/brokering/orders", name: "Brokering: Orders", component: Orders },
      { path: "/brokering/brokers", name: "Brokering: Brokers", component: Brokers },
      { path: "/brokering/history", name: "Brokering: History", component: History }
    ]
  },
  { path: "/customers", name: "Customers", component: Customers },
  {
    collapse: true,
    path: "/financials",
    name: "Financials",
    icon: DashboardIcon,
    state: "openFinancials",
    views: [
      { path: "/financials/overview", name: "Financials: Overview", component: Overview },
      { path: "/financials/invoices", name: "Financials: Invoices", component: Invoices },
      { path: "/financials/employees", name: "Financials: Employees", component: Employees }
    ]
  },
  { path: "/dispatch", name: "Dispatch", component: Dispatch }
];


