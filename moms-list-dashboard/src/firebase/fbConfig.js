import * as firebase from 'firebase'
import 'firebase/firestore' 
import 'firebase/auth' 

// Initialize Firebase
var config = {
  apiKey: "AIzaSyB3Ch94wjVNZGsMWfY5awH3M5fbTzjvZWA",
  authDomain: "moms-list.firebaseapp.com",
  databaseURL: "https://moms-list.firebaseio.com",
  projectId: "moms-list",
  storageBucket: "moms-list.appspot.com",
  messagingSenderId: "40874343115"
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true }); 
const googleAuthProvider = new firebase.auth.GoogleAuthProvider() 

export {firebase, googleAuthProvider, firebase as default};