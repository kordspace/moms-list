import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import { createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { reduxFirestore, getFirestore } from 'redux-firestore'
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase'
import { routerMiddleware } from 'connected-react-router'
import { ConnectedRouter } from 'connected-react-router'

import { firebase } from './firebase/fbConfig'
import { login, logout } from './store/actions/authActions'
import rootReducer from './store/reducers/rootReducer'
import indexRoutes from "routes/index.jsx";
import fbConfig from './firebase/fbConfig'

import "assets/scss/material-dashboard-pro-react.css?v=1.4.0";


const hist = createBrowserHistory();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer(hist),
  composeEnhancers(
    applyMiddleware(
      thunk.withExtraArgument({ getFirebase, getFirestore }),
      routerMiddleware(hist)
    ),
    reduxFirestore(fbConfig),
    reactReduxFirebase(fbConfig)
  )
);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={hist}>
      <Router history={hist}>
        <Switch>
          {indexRoutes.map((prop, key) => {
            return <Route path={prop.path} component={prop.component} key={key} />;
          })}
        </Switch>
      </Router>
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
);

firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    // Name, email address, and profile photo Url
    store.dispatch(login(user))
  } else {
    store.dispatch(logout())
  }
})
